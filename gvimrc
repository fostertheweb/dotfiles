set guifont=Source\ Code\ Pro:h14
set guioptions-=r
set guioptions-=L
set noshowmode

macmenu File.New\ Tab key=<nop>
macmenu File.Open\.\.\. key=<nop>
nnoremap <D-t> :CtrlP<CR>
